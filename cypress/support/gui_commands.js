/// <reference types="Cypress" />

Cypress.Commands.add('login', function() {
    cy.visit('users/sign_in')
    cy.get('#user_login').type(Cypress.env('user_name'))
    cy.get('#user_password').type(Cypress.env('user_password'))
    cy.get('#new_user > .submit-container > .btn').click()
  })

  Cypress.Commands.add('logout', () => {
    cy.get('.qa-user-avatar').click()
    cy.contains('Sign out').click()
  })

  Cypress.Commands.add('gui_createProject', project => {
    cy.visit('projects/new')
    cy.get('#project_name').type(project.name)
    cy.get(':nth-child(5) > #project_description').type(project.description)
    cy.get('#project_initialize_with_readme').check()
    cy.get('#blank-project-pane > #new_project > .btn-success').click()
  })

  Cypress.Commands.add('gui_createIssue', issue => {
    cy.visit(`${Cypress.env('user_name')}/${issue.project.name}/issues/new`)
    cy.get('#issue_title').type(issue.title)
    cy.get('#issue_description').type(issue.description)
    cy.get('.append-right-10 > .btn').click()
  })

  Cypress.Commands.add('gui_setLabelOnIssue', label => {
    cy.get('.qa-edit-link-labels').click()
    cy.contains(label.name).click()
    cy.get('body').click()
  })

  Cypress.Commands.add('gui_setMilestoneOnIssue', milestone => {
  cy.get('.block.milestone .edit-link').click()
  cy.contains(milestone.title).click()
})

  